var searchData=
[
  ['never_0',['Never',['../class_spawner.html#af06845e7b4df23c86c6b327831023631a6e7b34fa59e1bd229b207892956dc41c',1,'Spawner']]],
  ['numberofalignmenttext_1',['numberOfAlignmentText',['../class_u_i_controller.html#a3c82399b304324a6c0dcb1546ee4f3ed',1,'UIController']]],
  ['numberofbotstext_2',['numberOfBotsText',['../class_u_i_controller.html#a0cf8ebb0bab1c297d00229df6fe4019b',1,'UIController']]],
  ['numberofcohesiontext_3',['numberOfCohesionText',['../class_u_i_controller.html#ae6abda061b5020732c3fce55a1eb5629',1,'UIController']]],
  ['numberofseparationtext_4',['numberOfSeparationText',['../class_u_i_controller.html#aff3e60b6dc82037aa76c568387e36ae6',1,'UIController']]],
  ['numbots_5',['numBots',['../classglobal_swarm.html#a12127ea08d747c79409cba3f71209d51',1,'globalSwarm']]],
  ['numflockmates_6',['numFlockmates',['../struct_boid_manager_1_1_boid_data.html#a4ce24e2c012592e737dba4f0b15e09c4',1,'BoidManager::BoidData']]],
  ['numperceivedflockmates_7',['numPerceivedFlockmates',['../class_boid.html#a2c70cf17544fe1e570f00b90dc941abb',1,'Boid']]]
];
