var searchData=
[
  ['numberofalignmenttext_0',['numberOfAlignmentText',['../class_u_i_controller.html#a3c82399b304324a6c0dcb1546ee4f3ed',1,'UIController']]],
  ['numberofbotstext_1',['numberOfBotsText',['../class_u_i_controller.html#a0cf8ebb0bab1c297d00229df6fe4019b',1,'UIController']]],
  ['numberofcohesiontext_2',['numberOfCohesionText',['../class_u_i_controller.html#ae6abda061b5020732c3fce55a1eb5629',1,'UIController']]],
  ['numberofseparationtext_3',['numberOfSeparationText',['../class_u_i_controller.html#aff3e60b6dc82037aa76c568387e36ae6',1,'UIController']]],
  ['numbots_4',['numBots',['../classglobal_swarm.html#a12127ea08d747c79409cba3f71209d51',1,'globalSwarm']]],
  ['numflockmates_5',['numFlockmates',['../struct_boid_manager_1_1_boid_data.html#a4ce24e2c012592e737dba4f0b15e09c4',1,'BoidManager::BoidData']]],
  ['numperceivedflockmates_6',['numPerceivedFlockmates',['../class_boid.html#a2c70cf17544fe1e570f00b90dc941abb',1,'Boid']]]
];
